import Image from 'next/image';

export const Background = () => {
  return (
    <div className='absolute top-0 left-0 z-[-1] w-full h-full flex justify-center items-center bg-black'>
      <Image
        src='/images/rain-drops.jpg'
        alt='rain-drops'
        fill
        className='object-cover opacity-50'
      />
    </div>
  );
};
