import type { Metadata } from 'next';
import { Oxygen } from 'next/font/google';

import { Background } from './components/Background';

import './globals.css';

const oxygen = Oxygen({ weight: ['400', '700'], subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'Weather App',
  description: 'European weather monitoring app',
  keywords: 'weather, monitoring, european, weather, app',
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang='en'>
      <body className={oxygen.className}>
        <Background />
        {children}
      </body>
    </html>
  );
}
